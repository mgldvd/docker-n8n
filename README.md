# Docker n8n

<p float="left">
  <img src="https://cdn.svgporn.com/logos/docker.svg" height="40" title="docker">
  <img src="https://www.svgrepo.com/show/304556/three-dots.svg" height="30" title="dots">
  <img src="https://seeklogo.com/images/N/n8n-io-logo-5A4E4DA35A-seeklogo.com.png" height="40" title="n8n">
  <!-- <img src="https://www.svgrepo.com/show/304556/three-dots.svg" height="30" title="dots"> -->
  <!-- <img src="https://cdn.svgporn.com/logos/python.svg" height="40" title="python"> -->
</p>

Docker + n8n

## 📍 Start

```bash
task up

# or

docker compose up -d
```

## 📍 Make change on `Dockerfile`

```bash
task build
```

## Taksfile Install (command)

```bash
sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d
```

## DOCS:
- https://taskfile.dev/