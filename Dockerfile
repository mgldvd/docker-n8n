# syntax = docker/dockerfile:1

#region base
ARG NODE_VERSION=20
FROM node:${NODE_VERSION}-slim as base
ENV NPM_CONFIG_LOGLEVEL errors

#region build
RUN apt update

RUN mkdir -p /app

WORKDIR /app

RUN npm update -g

RUN npm i -g n8n

COPY ./app /app

#region release
EXPOSE 5678

ENTRYPOINT ["n8n", "start"]
